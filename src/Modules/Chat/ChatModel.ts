import {mongoose} from "../../Services/Database";
import {IChatData} from "./ChatData";
import {ChatSchema} from "./ChatSchema";

let ChatModel;

try {
    // Throws an error if 'Name' hasn't been registered
    ChatModel = mongoose.model('Chat')
} catch (e) {
    ChatModel = mongoose.model<IChatData>('Chat', ChatSchema);
}

export = ChatModel;