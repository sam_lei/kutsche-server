import {Response} from 'express';

/**
 * pseudo extends Response
 */
export class ApiResponse {
    private code: number;
    private data: any;

    /**
     * new ApiResponse
     * @param code
     * @param data
     */
    constructor(code: number, data?: any) {
        this.code = code;
        this.data = data ? data : null;
    }

    /**
     * add this to existing Response
     * @param response
     * @param log
     * @returns {Response}
     */
    public addToResponse(response: Response, log?: string): Response {
        response.statusCode = this.code;
        response.json(this.data);
        if (log) console.log(log);

        return response;
    }

    /**
     * is http code 200
     * @returns {boolean}
     */
    public isSuccess(): boolean {
        return (this.code == 200);
    }

    /**
     * is http code 500
     * @returns {boolean}
     */
    public isError(): boolean {
        return (this.code == 500);
    }

    /**
     * ApiResponse code
     * @returns {number}
     */
    public getCode(): number {
        return this.code;
    }

    /**
     * ApiResponse data
     * @returns {object | string | null}
     */
    public getData(): any {
        return this.data;
    }
}