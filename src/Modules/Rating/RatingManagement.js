"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiResponse_1 = require("../../Services/ApiResponse");
var UserManagement_1 = require("../User/UserManagement");
var RatingManagement = (function () {
    function RatingManagement() {
        this.userManagement = new UserManagement_1.UserManagement();
    }
    /**
     * returns number of ratings
     * @param ratings
     * @returns {number}
     */
    RatingManagement.getNumber = function (ratings) {
        return ratings.length;
    };
    /**
     * returns summ of ratings
     * @param ratings
     * @returns {number}
     */
    RatingManagement.prototype.getSumm = function (ratings) {
        var summ = 0;
        ratings.forEach(function (rating) { summ = summ + rating.rating; });
        return summ;
    };
    /**
     * returns average rating
     * @param ratings
     * @returns {number}
     */
    RatingManagement.prototype.getAverage = function (ratings) {
        return (this.getSumm(ratings) / RatingManagement.getNumber(ratings));
    };
    RatingManagement.prototype.putRating = function (ratedId, ratingUser, rating) {
        var _this = this;
        return this.userManagement.getPrivateUser(ratedId).then(function (ratedUser) {
            if (!ratingUser) {
                return new ApiResponse_1.ApiResponse(300, 'Nicht angemeldet');
            }
            rating.date = new Date();
            rating.from = ratingUser._id;
            rating.rating = rating.rating ? rating.rating : 100;
            // find index of rating on this user from rater
            var i = ratedUser.ratings.findIndex(function (rating) {
                return rating.from == ratingUser._id;
            });
            // add new to front
            if (i == -1)
                ratedUser.ratings.unshift(rating);
            else {
                // remove old
                ratedUser.ratings.splice(i, 1);
                // add edited to front
                ratedUser.ratings.unshift(rating);
            }
            return _this.userManagement.putUser(ratedUser, ratedId);
        });
    };
    RatingManagement.prototype.getRating = function (ratedId, ratingUser) {
        return this.userManagement.getPrivateUser(ratedId).then(function (ratedUser) {
            // find index of rating on this user from rater
            var i = ratedUser.ratings.findIndex(function (rating) { return rating.from == ratingUser._id; });
            // nothing exits
            if (i == -1)
                return new ApiResponse_1.ApiResponse(204, null);
            else
                return new ApiResponse_1.ApiResponse(200, ratedUser.ratings[i]);
        });
    };
    return RatingManagement;
}());
exports.RatingManagement = RatingManagement;
//# sourceMappingURL=RatingManagement.js.map