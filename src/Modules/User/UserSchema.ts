import { mongoose } from "../../Services/Database";
import * as passportLocalMongoose from 'passport-local-mongoose';
import { UserInfoSchema } from "./Info/UserInfoSchema";
import { RatingSchema } from "../Rating/RatingSchema";
import { UserCarSchema } from "./Car/UserCarSchema";

export const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    lastLogin: {
        type: Date,
        required: false
    },
    verified: {
        type: Boolean,
        required: true
    },
    resetPasswordToken: {
        type: String,
        required: false
    },
    resetPasswordExpires: {
        type: Number,
        required: false
    },
    kutscheServedCount: {
        type: Number,
        required: true
    },
    kutscheTakenCount: {
        type: Number,
        required: true
    },
    ratings: {
        type: [RatingSchema],
        required: false
    },
    privateInfo: {
        type: UserInfoSchema,
        required: true
    },
    publicInfo: {
        type: UserInfoSchema,
        required: true
    },
    car: {
        type: UserCarSchema,
        required: false
    }
});


/**
 * join with plugin schema from passport
 * https://github.com/saintedlama/passport-local-mongoose
 */
UserSchema.plugin(passportLocalMongoose, {
    interval: 1000,
    usernameField: 'email',
    usernameLowerCase: true,
    lastLoginField: 'lastLogin'
});