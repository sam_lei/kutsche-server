import { SocketData } from "./SocketData";
import { MessageData } from "../Chat/Message/MessageData";

export class SocketManagement {
    private static io: any;
    private static connections: SocketData[];

    /**
     * Singleton *hust*
     * @param io
     */
    constructor(io: any) {
        SocketManagement.connections = [];
        SocketManagement.io = io;
        SocketManagement.io.set('transports', [ 'websocket' ]);
        SocketManagement.listen();
        console.log('socket-init');
    }

    private static listen(): void {
        SocketManagement.io.on('connect', (socket: any) => {
            // console.log('Connected client %s.', socket.id);

            // TODO: Authentication
            socket.on('handshake', (m: any) => {
                // console.log('handshake: [user.id]: %s', m);
                this.addConnection(socket.id, m);
                /*
                let msg: MessageData = {
                    to: 'me',
                    from: 'me',
                    text: 'hi'
                };
                let i = setInterval(() => {
                    this.sendToUser(m, msg);
                }, 4000); */
            });

            socket.on('disconnect', () => {
                // console.log('Client disconnected');
                this.removeConnection(socket.id);
            });
        });
    }

    private static addConnection(socketId: string, userId: string) {
        if (socketId === '' || userId === '')
            return;

        // remove old sockets with same userID
        for (let i = 0; i < SocketManagement.connections.length; i++) {
            if (SocketManagement.connections[i].userId === userId) {
                SocketManagement.connections.splice(i, 1);
            }
        }

        let connection: SocketData = {
            socketId: socketId,
            userId: userId,
            connected: true
        };
        SocketManagement.connections.push(connection);

        // MessageManagement.sendToUser(userId, "Willkommen");
        console.log('connected sockets: ' + SocketManagement.connections.length);
    }

    private static removeConnection(socketId: string) {
        for (let i = 0; i < SocketManagement.connections.length; i++) {
            if (SocketManagement.connections[i].socketId === socketId) {
                SocketManagement.connections.splice(i, 1);
            }
        }
        // console.log(SocketManagement.connections);
    }

    public static sendToUser(userId: string, message: MessageData | string) {
        // gehe array mit socketID <-> userID paaren durch
        for (let i = 0; i < this.connections.length; i++) {
            // userID ist gerade verbunden
            if (this.connections[i].userId === userId) {
                SocketManagement.io.to(this.connections[i].socketId).emit('message', message);
                console.log('socket send: ' + message);
            }
        }
    }
}