import {Router, Request, Response} from 'express';
import {UserRouter} from './UserRouter';
import {ChatManagement} from "../Modules/Chat/ChatManagement";
import {RatingManagement} from "../Modules/Rating/RatingManagement";

/**
 * rating
 */
export class RatingRouter {
    private router: Router = Router();

    getRouter(): Router {
        let ratingManagement = new RatingManagement();
        let userRouter = new UserRouter();

        this.router.get('/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            ratingManagement.getRating(request.params.id, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.put('/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            ratingManagement.putRating(request.params.id, request.user, request.body).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        return this.router;
    }
}

