# Kutschen Server

### Queries
Start shell:
```
mongo
```
Select Database:
```
use data
```
Show all Users:
```
db.users.find({}, { "privateInfo.name": 1, "email": 1 })
```
Set verified:
```
db.users.update({"email": "test@test.de"}, {$set: {"verified": true}});
```
Change email:
```
db.users.updateOne({"email": "test@test.de"}, { $set: {"email": "test2@test.de"}})

```

### deployment
* upload to server
* login as samuel on server
* pm2 restart kutschen

### MongoDB
Start:
```
mongod --port 27017 --fork --logpath ~/log/mongod.logx --dbpath /var/lib/mongodb/
```
Stop:
```
sudo service mongod stop
sudo killall mongod

```