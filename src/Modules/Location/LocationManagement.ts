import {LocationData} from "./LocationData";
import {URLSearchParams} from "url";
import * as http from "http";

export class LocationManagement {

    constructor() {
    }

    /**
     * calculate trigonometrical distance between two locations in km
     * @param location1
     * @param location2
     * @returns {number}
     */
    public static getDistance(location1: LocationData, location2: LocationData): number {
        let earthRadius = 6378.137; // km
        let dLat = location2.lat * Math.PI / 180 - location1.lat * Math.PI / 180;
        let dLon = location2.long * Math.PI / 180 - location1.long * Math.PI / 180;
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(location1.lat * Math.PI / 180) * Math.cos(location2.lat * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return earthRadius * c;
    }

    public static latToKm(lat1: number, lat2: number): number {
        return Math.abs(lat1 - lat2) * 110.574;
    }

    public static longToKm(long1: number, long2: number, lat: number): number {
        return Math.abs(long1 - long2) * 111.32 * Math.cos(lat);
    }

    /**
     * location 1 and location 2 are in distance
     * @param distance
     * @param location1
     * @param location2
     * @returns {boolean}
     */
    public static inDistance(distance: number, location1: LocationData, location2: LocationData): boolean {
        return (LocationManagement.getDistance(location1, location2) <= distance);
    }
}