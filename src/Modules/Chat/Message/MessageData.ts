export interface MessageData {
    to: string;
    from: string;
    text: string;
    read?: boolean;
    date?: Date;
    senderName?: string;
}