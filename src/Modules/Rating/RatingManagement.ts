import {RatingData} from "./RatingData";
import {IUserData} from "../User/UserData";
import {ApiResponse} from "../../Services/ApiResponse";
import {UserManagement} from "../User/UserManagement";

export class RatingManagement {
    private userManagement = new UserManagement();

    /**
     * returns number of ratings
     * @param ratings
     * @returns {number}
     */
    private static getNumber(ratings: RatingData[]): number {
        return ratings.length;
    }

    /**
     * returns summ of ratings
     * @param ratings
     * @returns {number}
     */
    private getSumm(ratings: RatingData[]): number {
        let summ: number = 0;
        ratings.forEach(rating => {summ = summ + rating.rating});

        return summ;
    }

    /**
     * returns average rating
     * @param ratings
     * @returns {number}
     */
    public getAverage(ratings: RatingData[]): number {
        return (this.getSumm(ratings) / RatingManagement.getNumber(ratings));
    }

    public putRating(ratedId: string, ratingUser: IUserData, rating: RatingData): Promise<ApiResponse> {
        return this.userManagement.getPrivateUser(ratedId).then((ratedUser: IUserData) => {
            if (!ratingUser) {
                return new ApiResponse(300, 'Nicht angemeldet');
            }

            rating.date = new Date();
            rating.from = ratingUser._id;
            rating.rating = rating.rating ? rating.rating : 100;

            // find index of rating on this user from rater
            let i = ratedUser.ratings.findIndex(rating => {
                return rating.from == ratingUser._id;
            });

            // add new to front
            if (i == -1)
                ratedUser.ratings.unshift(rating);
            // edit old
            else {
                // remove old
                ratedUser.ratings.splice(i, 1);
                // add edited to front
                ratedUser.ratings.unshift(rating);
            }

            return this.userManagement.putUser(ratedUser, ratedId);
        });
    }

    public getRating(ratedId: string, ratingUser: IUserData): Promise<ApiResponse> {
        return this.userManagement.getPrivateUser(ratedId).then((ratedUser: IUserData) => {

            // find index of rating on this user from rater
            let i = ratedUser.ratings.findIndex(rating => rating.from == ratingUser._id);

            // nothing exits
            if (i == -1)
                return new ApiResponse(204, null);
            // found one
            else
                return new ApiResponse(200, ratedUser.ratings[i]);
        });
    }
}