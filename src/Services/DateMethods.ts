/**
 * custom methods for native Date object
 */
export class DateMethods {

    /**
     * add seconds to date
     * @param date
     * @param seconds
     * @returns {Date}
     */
    public static addSeconds(date: Date, seconds: number): Date {
        let result = new Date(date);
        result.setSeconds(result.getSeconds() + seconds);
        return result;
    }

    /**
     * add hours to date
     * @param date
     * @param hours
     * @returns {Date}
     */
    public static addHours(date: Date, hours: number): Date {
        let result = new Date(date);
        result.setHours(result.getHours() + hours);
        return result;
    }

    /**
     * add days to date
     * @param date
     * @param days
     * @returns {Date}
     */
    public static addDays(date: Date, days: number): Date {
        let result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }
}