import {mongoose} from "../../Services/Database";

export const LocationSchema = new mongoose.Schema({
    lat: {
        type: Number,
        required: true
    },
    long: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: Object,
        required: false
    },
    searched: {
        type: Boolean,
        default: false,
        required: true
    }
});