import {mongoose} from "../../Services/Database";
import {IUserData} from './UserData';
import {UserSchema} from "./UserSchema";

let UserModel;

try {
    // Throws an error if 'Name' hasn't been registered
    UserModel = mongoose.model('User')
} catch (e) {
    UserModel = mongoose.model<IUserData>('User', UserSchema);
}

export = UserModel;