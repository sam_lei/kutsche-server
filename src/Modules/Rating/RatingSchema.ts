import {mongoose} from "../../Services/Database";

export const RatingSchema = new mongoose.Schema({
    from: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    text: {
        type: String,
        required: false
    }
});