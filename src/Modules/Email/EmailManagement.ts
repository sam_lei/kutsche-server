import * as nodemailer from 'nodemailer';

export class EmailManagement {
    private static transporter: any;

    /**
     * Singleton *hust*
     */
    constructor() {
        // create reusable transporter object using the default SMTP transport
        EmailManagement.transporter = nodemailer.createTransport({
            host: 'smtp.kutschen.org',
            port: 25,
            secure: false, // secure:true for port 465, secure:false for port 587
            requireTLS: true,
            auth: {
                user: 'kontakt@kutschen.org',
                pass: 'KontaktNo1z3e88Kutschen'
            },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false
            },
            logger: false
        });
        // verify connection configuration
        EmailManagement.transporter.verify((error: any, success: any) => {
            if (error) {
                console.log(error);
            } else {
                console.log('Server is ready to take our messages');
            }
        });
        // EmailManagement.sendEmail('samuel@leihkamm.net', 'Test', 'Inhalt');
    }

    public static sendEmail(email: string, subject: string, message: string): void {
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Kutschen" <kontakt@kutschen.org>', // sender address
            to: email, // list of receivers
            subject: subject, // Subject line
            text: message // plain text body
        };
        // send mail with defined transport object
        EmailManagement.transporter.sendMail(mailOptions, (error: any, info: any) => {
            if (error)
                console.log(error);
            else
                console.log('Message %s sent: %s', info.messageId, info.response);
        });
    }
}