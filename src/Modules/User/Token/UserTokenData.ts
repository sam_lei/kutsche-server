import {UserData} from "../UserData";


interface UserAuth {
    token: string;
    issued: Date;
    expires: Date;
}

export interface UserTokenData {
    user: UserData;
    auth: UserAuth;
}