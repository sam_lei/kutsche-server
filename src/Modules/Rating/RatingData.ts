import { mongoose } from "../../Services/Database";

export interface RatingData {
    from: string;
    rating: number;
    date: Date;
    text: string;
}

export interface IRatingData extends RatingData, mongoose.Document {
}