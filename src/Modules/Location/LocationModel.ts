import {mongoose} from "../../Services/Database";
import {ILocationData} from "./LocationData";
import {LocationSchema} from "./LocationSchema";

let LocationModel;

try {
    // Throws an error if 'Name' hasn't been registered
    LocationModel = mongoose.model('Location')
} catch (e) {
    LocationModel = mongoose.model<ILocationData>('Location', LocationSchema);
}

export = LocationModel;