import {mongoose} from "../../../Services/Database";
import {LocationSchema} from "../../Location/LocationSchema";

export const UserInfoSchema = new mongoose.Schema({
    image: {
        type: String,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    telegram: {
        type: String,
        required: false
    },
    adress: {
        type: LocationSchema,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    birthDate: {
        type: Date,
        required: false
    },
});