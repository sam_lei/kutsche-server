import {mongoose} from "../../Services/Database";
import {UserData} from "../User/UserData";
import {LocationData} from "../Location/LocationData";

export interface KutscheData {
    user?: string;
    date: Date;
    stops: LocationData[];
    guests?: string[];
    seats: number;
    description?: string;
    price: number;
}

export interface IKutscheData extends KutscheData, mongoose.Document { }
