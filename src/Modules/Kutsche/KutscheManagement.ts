import { IKutscheData, KutscheData } from "./KutscheData";
import { IUserData } from "../User/UserData";
import { LocationData } from "../Location/LocationData";
import { LocationManagement } from "../Location/LocationManagement";
import { DateMethods } from "../../Services/DateMethods";
import { ApiResponse } from "../../Services/ApiResponse";
import * as KutscheModel from "./KutscheModel";
import { UserManagement } from "../User/UserManagement";
import { DistanceMethods } from "../../Services/DistanceMethods";

export class KutscheManagement {
    private userManagement: UserManagement;

    constructor() {
        this.userManagement = new UserManagement();
    }

    /**
     * create most used ApiResponses from database call
     * @param dataManipulation
     * @returns {Promise<ApiResponse>}
     */
    private basicResponse(dataManipulation: Promise<any>): Promise<ApiResponse> {
        return dataManipulation.then(
            (docs: any) => {
                return new ApiResponse(200, docs);
            },
            (err: any) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * user created kutsche
     * @param kutscheId
     * @param user
     * @returns {Promise<boolean>}
     */
    private async isOwner(kutscheId: string, user: IUserData): Promise<boolean> {
        let isOwner = false;
        await KutscheModel.findOne({'_id': kutscheId}).then((kutsche: IKutscheData) => {
            if (kutsche && kutsche.user)
                isOwner = (kutsche.user == user._id);
        });

        return isOwner;
    }

    /**
     * POST new kutsche
     * @param kutsche
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public add(kutsche: KutscheData, user: IUserData): Promise<ApiResponse> {
        // add current User to Kutsche
        kutsche.user = user._id;

        let dataCreate = KutscheModel.create(kutsche);
        UserManagement.increaseKutscheServedCount(user);
        return this.basicResponse(dataCreate);
    }

    /**
     * DELETE kutsche by id
     * @param id
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public async remove(id: string, user: IUserData): Promise<ApiResponse> {
        // validate ownership
        if (await this.isOwner(id, user) == false)
            return new ApiResponse(403, 'no ownership');

        UserManagement.decreaseKutscheServedCount(user);
        let dataRemoval = KutscheModel.findByIdAndRemove(id);
        return this.basicResponse(dataRemoval);
    }

    /**
     * UPDATE kutsche by id
     * @param id
     * @param kutsche
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public async update(id: string, kutsche: KutscheData, user: IUserData): Promise<ApiResponse> {
        // validate ownership
        if (await this.isOwner(id, user) == false)
            return new ApiResponse(403, 'no ownership');

        // add current User to Kutsche
        kutsche.user = user._id;

        let dataUpdate = KutscheModel.findByIdAndUpdate(id, kutsche, {'new': true}); // new flag returns modified kutsche
        return this.basicResponse(dataUpdate);
    }

    /**
     * ADD guest to kutsche by guest user id and kutsche id
     * @param kutscheId
     * @param guestId
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public async addGuest(kutscheId: string, guestId: string, user: IUserData): Promise<ApiResponse> {
        // validate ownership
        if (await this.isOwner(kutscheId, user) == false)
            return new ApiResponse(403, 'no ownership');

        return KutscheModel.findOne({'_id': kutscheId}).then(
            (kutsche: IKutscheData) => {
                // add guest
                if (!kutsche.guests)
                    return new ApiResponse(404, 'kutsche guests nicht gefunden');

                if (kutsche.guests.indexOf(guestId, 0) === -1) {
                    UserManagement.increaseKutscheTakenCount(user);
                    kutsche.guests.push(guestId);
                }
                let dataUpdate = KutscheModel.findByIdAndUpdate(kutscheId, kutsche, {'new': true}); // new flag returns modified kutsche
                return this.basicResponse(dataUpdate);
            });
    }

    /**
     * REMOVE guest from kutsche by guest user id and kutsche id
     * @param kutscheId
     * @param guestId
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public async removeGuest(kutscheId: string, guestId: string, user: IUserData): Promise<ApiResponse> {
        // validate ownership
        if (await this.isOwner(kutscheId, user) === false)
            return new ApiResponse(403, 'no ownership');

        return KutscheModel.findOne({'_id': kutscheId}).then(
            (kutsche: IKutscheData) => {
                // add guest
                if (!kutsche.guests)
                    return new ApiResponse(404, 'kutsche guests nicht gefunden');

                let index = kutsche.guests.indexOf(guestId, 0);
                if (index > -1) {
                    UserManagement.decreaseKutscheTakenCount(user);
                    kutsche.guests.splice(index, 1);
                }
                let dataUpdate = KutscheModel.findByIdAndUpdate(kutscheId, kutsche, {'new': true}); // new flag returns modified kutsche
                return this.basicResponse(dataUpdate);
            });
    }

    /**
     * GET all kutsches
     * @returns {Promise<ApiResponse>}
     */
    public getAll(): Promise<ApiResponse> {
        let dataManipulation = KutscheModel.find({
            'date': {
                "$gte": new Date()
            }
        }).limit(200).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        });
        return this.basicResponse(dataManipulation);
    }

    /**
     * GET kutsche by ID
     * @param id
     * @returns {Promise<ApiResponse>}
     */
    public getById(id: string): Promise<ApiResponse> {
        let dataManipulation = KutscheModel.findOne({'_id': id}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        });
        return this.basicResponse(dataManipulation);
    }

    /**
     * GET kutschen by userID
     * @param id
     * @returns {Promise<ApiResponse>}
     */
    public getByUserId(id: string): Promise<ApiResponse> {
        let dataManipulation = KutscheModel.find({
            'user': id,
            'date': {
                "$gte": DateMethods.addDays(new Date(), -10),
            }
        }).limit(200).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        });
        return this.basicResponse(dataManipulation);
    }

    /**
     * GET kutschen by guestID
     * @param id
     * @returns {Promise<ApiResponse>}
     */
    public getByGuestId(id: string): Promise<ApiResponse> {
        let dataManipulation = KutscheModel.find({
            'guests': id,
            'date': {
                "$gte": DateMethods.addDays(new Date(), -10),
            }
        }).limit(200).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        });
        return this.basicResponse(dataManipulation);
    }

    /**
     * GET kutsche by exact date
     * @param date
     * @returns {Promise<ApiResponse>}
     */
    public getByDate(date: Date): Promise<ApiResponse> {
        let kutscheSearch = KutscheModel.find({'date': date}).limit(100).sort({date: 'asc'}).populate({
            path: 'user',

            select: '-privateInfo -email'
        });
        return this.basicResponse(kutscheSearch);
    }

    /**
     * GET kutsches from now to xx days in future
     * @param days
     * @returns {Promise<ApiResponse>}
     */
    public getByFuture(days: number): Promise<ApiResponse> {
        let today: Date = new Date();
        let future: Date = DateMethods.addDays(today, days);
        let kutscheSearch = KutscheModel.find({
            'date': {
                "$gte": today,
                "$lt": future
            }
        }).limit(200).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        });;

        return this.basicResponse(kutscheSearch);
    }

    /**
     * GET kutsches in distance from location
     * @param distance
     * @param location
     * @returns {Promise<ApiResponse>}
     */
    public getByStart(distance: number, location: LocationData): Promise<ApiResponse> {
        return KutscheModel.find({
            'date': {
                "$gte": new Date()
            }
        }).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        }).then(
            (kutschen: IKutscheData[]) => {
                let data = KutscheManagement.filterByStart(kutschen, distance, location);
                return new ApiResponse(200, data);
            },
            (err: Error) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * GET kutsches in distance from location
     * @param distance
     * @param location
     * @returns {Promise<ApiResponse>}
     */
    public getByStop(distance: number, location: LocationData): Promise<ApiResponse> {
        return KutscheModel.find({
            'date': {
                "$gte": new Date()
            }
        }).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        }).then(
            (kutschen: IKutscheData[]) => {
                let data = KutscheManagement.filterByStop(kutschen, distance, location);
                return new ApiResponse(200, data);
            },
            (err: Error) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * GET kutsches in distance from location
     * @param filter
     * @returns {Promise<ApiResponse>}
     */
    public getByFilter(filter: any): Promise<ApiResponse> {
        // console.log(filter);


        if (filter.start && filter.start.lat === 0 && filter.stop && filter.stop.lat === 0)
            return this.getAll();

        if (filter.start && filter.start.lat === 0)
            return this.getByStop(filter.range, filter.stop);

        if (filter.stop && filter.stop.lat === 0)
            return this.getByStart(filter.range, filter.start);

        return KutscheModel.find({
            'date': {
                "$gte": new Date()
            }
        }).limit(100).sort({date: 'asc'}).populate({
            path: 'user',
            select: '-privateInfo -email'
        }).populate({
            path: 'guests',
            select: 'publicInfo.name'
        }).then(
            (kutschen: IKutscheData[]) => {
                let data = KutscheManagement.filterByStartStop(kutschen, filter);
                return new ApiResponse(200, data);
            },
            (err: Error) => {
                return new ApiResponse(500, err);
            });
    }


    /**
     * GET statistics
     * @returns {Promise<ApiResponse>}
     */
    public getStatistics(): Promise<ApiResponse> {
        return KutscheModel.find().then(
            (kutschen: IKutscheData[]) => {
                let data = KutscheManagement.calcStatistics(kutschen);
                return new ApiResponse(200, data);
            },
            (err: Error) => {
                return new ApiResponse(500, err);
            });
    }

    private static filterByStartStop(kutschen: IKutscheData[], filter: any): IKutscheData[] {
        if (!kutschen) return [];
        let data: IKutscheData[] = [];

        // add to response if any stop and start of kutsche is in distance
        for (let i = 0; i < kutschen.length; i++) {
            let start = false;
            let stop = false;

            for (let n = 0; n < kutschen[i].stops.length; n++) {
                if (!start) {
                    start = LocationManagement
                        .inDistance(filter.range, filter.start, kutschen[i].stops[n]);
                    kutschen[i].stops[n].searched = start;
                }
                else {
                    stop = LocationManagement
                        .inDistance(filter.range, filter.stop, kutschen[i].stops[n]);
                    kutschen[i].stops[n].searched = stop;
                }
                if (start && stop) break;
            }

            if (start && stop) data.push(kutschen[i]);
        }

        return data;
    }

    private static filterByStart(kutschen: IKutscheData[], distance: number, location: LocationData): IKutscheData[] {
        if (!kutschen) return [];
        let data: IKutscheData[] = [];

        // add to response if any except last stop of kutsche is in distance
        for (let i = 0; i < kutschen.length; i++) {

            for (let n = 0; n < kutschen[i].stops.length - 1; n++) {

                if (LocationManagement.inDistance(distance, location, kutschen[i].stops[n])) {
                    kutschen[i].stops[n].searched = true;
                    data.push(kutschen[i]);
                    break;
                }
            }
        }

        return data;
    }

    private static filterByStop(kutschen: IKutscheData[], distance: number, location: LocationData): IKutscheData[] {
        if (!kutschen) return [];
        let data: IKutscheData[] = [];

        // add to response if any stop of kutsche is in distance
        for (let i = 0; i < kutschen.length; i++) {

            for (let n = 0; n < kutschen[i].stops.length; n++) {

                if (LocationManagement.inDistance(distance, location, kutschen[i].stops[n])) {
                    kutschen[i].stops[n].searched = true;
                    data.push(kutschen[i]);
                    break;
                }
            }
        }

        return data;
    }

    private static calcStatistics(kutschen: IKutscheData[]): Object {
        let distance: number = 0;
        let locations: string[] = [];

        for (let kutsche of kutschen) {
            if (kutsche.guests && kutsche.guests.length > 0)
                distance += DistanceMethods.getDistance(kutsche.stops[0], kutsche.stops[kutsche.stops.length - 1]) * kutsche.guests.length;

            for (let stop of kutsche.stops) {
                if (stop && stop.address) {
                    if (stop.address.city)
                        locations.push(stop.address.city);

                    else if (stop.address.town)
                        locations.push(stop.address.town);

                    else if (stop.address.village)
                        locations.push(stop.address.village);
                }
            }
        }
        return {
            CO2InKg: Math.round(distance * DistanceMethods.C02Factor),
            distanceInKm: Math.round(distance),
            moneyInEur: Math.round(distance * DistanceMethods.MoneyFactor),
            locations: locations
        }
    }
}