export interface SocketData {
    socketId: string;
    userId: string;
    connected: boolean;
}