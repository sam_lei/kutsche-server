import {mongoose} from "../../Services/Database";

export interface LocationData {
    lat: number;
    long: number;
    name: string;
    address: Address;
    searched: boolean;
}

export interface Address {
    city?: string;
    county?: string;
    state_district?: string;
    state?: string;
    country?: string;
    country_code?: string;
    hamlet?: string;
    village?: string;
    residential?: string;
    suburb?: string;
    town?: string;
    house_number?: string;
    road?: string;
    region?: string;
    peak?: string;
    locality?: string;
    city_district?: string;
    postcode?: string;
    continent?: string;
}

export interface ILocationData extends LocationData, mongoose.Document { }
