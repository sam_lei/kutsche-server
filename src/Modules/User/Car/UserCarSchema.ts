import { mongoose } from "../../../Services/Database";

export const UserCarSchema = new mongoose.Schema({
    color: {
        type: String,
        required: false
    },
    seats: {
        type: Number,
        required: false
    },
    aircondition: {
        type: Boolean,
        required: false
    },
    hybrid: {
        type: Boolean,
        required: false
    },
    electric: {
        type: Boolean,
        required: false
    },
    manufacturer: {
        type: String,
        required: false
    }
});