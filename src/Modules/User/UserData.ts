import {mongoose} from "../../Services/Database";
import {UserDataInfo} from "./Info/UserInfoData";
import {RatingData} from "../Rating/RatingData";
import { UserCarData } from "./Car/UserCarData";

export interface UserData {
    password?: string;
    salt?: string;
    hash?: string;
    email: string;
    verified?: boolean;
    lastLoggedIn?: Date;
    resetPasswordToken?: String;
    resetPasswordExpires?: Number;
    kutscheServedCount: number;
    kutscheTakenCount: number;
    ratings: RatingData[];
    // auth stuff is added by Passport
    privateInfo: UserDataInfo;
    publicInfo: UserDataInfo;
    car?: UserCarData;
}

export interface IUserData extends UserData, mongoose.Document, mongoose.PassportLocalDocument { }
