export interface UserCarData {
    color?: string;
    seats?: number;
    aircondition?: boolean;
    hybrid?: boolean;
    electric?: boolean;
    manufacturer?: string;
}