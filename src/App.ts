#!/usr/bin/env nodejs

import * as express from "express";
import * as http from "http";
import * as socketIo from "socket.io";
import * as bodyParser from 'body-parser';

import { UserRouter } from './Routes/UserRouter';
import { KutscheRouter } from "./Routes/KutscheRoutes";
import { ChatRouter } from "./Routes/ChatRouter";
import { RatingRouter } from "./Routes/RatingRouter";
import { SocketManagement } from "./Modules/Socket/SocketManagement";
import { EmailManagement } from "./Modules/Email/EmailManagement";

class Server {
    public static readonly PORT = 2017;
    private app: express.Application;
    private server: any;
    private port: number;

    public static bootstrap(): Server {
        return new Server();
    }

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.addHeaders();
        this.createRoutes();
        this.sockets();
        this.email();
        this.listenApi();
    }

    private createApp(): void {
        this.app = express();
    }

    private addHeaders(): void {
        this.app.use((request, response, next) => {
            /* Website you wish to allow to connect
            let allowedOrigins = ['http://localhost:4200', 'https://kutschen.org', 'https://*.kutschen.org'];
            let origin = request.headers.origin;
            if (origin && typeof origin === 'string' && allowedOrigins.indexOf(origin) > -1) {
                response.setHeader('Access-Control-Allow-Origin', origin);
            } */
            response.setHeader('Access-Control-Allow-Origin', '*');

            // Request methods you wish to allow
            response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            // Request headers you wish to allow
            response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
            // Set to true if you need the website to include cookies in the requests sent
            // to the API (e.g. in case you use sessions)
            response.setHeader('Access-Control-Allow-Credentials', 'false');
            // Pass to next layer of middleware
            next();
        });
    }

    private createRoutes(): void {
        this.app.get('/', (req, res) => res.json({'name': 'Server is running'}));

        // imitate slow network
        // this.app.use((req, res, next) => setTimeout(next, 200));

        this.app.use('/photos/', express.static('../photos'));

        this.app.use('/user/', new UserRouter().getRouter());
        this.app.use('/kutsche/', new KutscheRouter().getRouter());
        this.app.use('/chat/', new ChatRouter().getRouter());
        this.app.use('/rating/', new RatingRouter().getRouter());
    }

    private createServer(): void {
        this.server = http.createServer(this.app);
    }

    private config(): void {
        this.port = Number(process.env.PORT) || Server.PORT;
        this.app.use(bodyParser.json());
    }

    private sockets(): void {
        let io = socketIo(this.server);
        new SocketManagement(io);
    }

    private email(): void {
        new EmailManagement();
    }

    private listenApi(): void {
        this.server.listen(this.port, () => {
            console.log('Server listening on port %d in %s mode', this.port, this.app.settings.env);
        });
    }

}

let server = Server.bootstrap();
export = server;