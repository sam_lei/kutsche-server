import {
    ChatData,
    IChatData
} from './ChatData';
import { IUserData } from '../User/UserData';
import { MessageData } from './Message/MessageData';
import * as ChatModel from './ChatModel';
import { ApiResponse } from '../../Services/ApiResponse';
import { SocketManagement } from '../Socket/SocketManagement';
import UserModel = require('../User/UserModel');

export class ChatManagement {

    /**
     * Add additional infos to message
     * @param message
     * @returns {MessageData}
     */
    private static prepareMessage(message: MessageData): MessageData {
        message.date = new Date();
        message.read = false;

        return message;
    }

    /**
     * create new chat including first message
     * @param idOne: UserID 1
     * @param idTwo: UserID 2
     * @param kutsche
     * @param message
     * @param user
     * @returns {Promise<IChatData>}
     */
    private static newChat(idOne: string, idTwo: string, kutsche: string, message: MessageData, user: IUserData): Promise<IChatData> {
        message = ChatManagement.prepareMessage(message);
        let chat: ChatData = {
            userOne: idOne,
            userTwo: idTwo,
            kutsche: kutsche,
            messages: [message]
        };

        return ChatModel.create(chat).then(() => {
            return this.getChat(idOne, idTwo, user);
        });
    }

    /**
     * returns chat by two UserIDs
     * @param idOne: UserID 1
     * @param idTwo: UserID 2
     * @param user
     * @returns {Promise<IChatData>}
     */
    private static getChat(idOne: string, idTwo: string, user: IUserData): Promise<IChatData> {
        return ChatModel.findOne({
            $or: [{
                'userOne': idOne,
                'userTwo': idTwo
            }, {
                'userOne': idTwo,
                'userTwo': idOne
            }]
        }).slice('messages', -50).populate({
            path: 'kutsche',
            select: 'stops date seats guests user'
        }).populate({
            path: 'userOne userTwo',
            select: 'publicInfo.name publicInfo.image'
        });
    }

    /**
     * returns chat by two UserIDs
     * @param id: userID
     * @returns {Promise<IChatData>}
     */
    private static getChats(id: string): Promise<IChatData[]> {
        return ChatModel.find({
            $or: [{
                'userOne': id
            }, {
                'userTwo': id
            }]
        }).limit(50).slice('messages', -1).populate({
            path: 'userOne userTwo',
            select: 'publicInfo.name publicInfo.image verified'
        });
    }

    /**
     * mark all messages to user._id in chat as read
     * @param idOther: UserID 1
     * @param user
     */
    public markChatAsRead(idOther: string, user: IUserData): Promise<any> {
        // TODO: replace with $elemMatch: multiple as soon as supported
        function markNextMsgAsRead() {
            return ChatModel.updateMany({
                $or: [{
                    'userOne': idOther,
                    'userTwo': user._id
                }, {
                    'userOne': user._id,
                    'userTwo': idOther
                }],
                'messages': {$elemMatch: {'to': user._id, 'read': false}}
            }, {
                $set: {
                    'messages.$.read': true
                }
            }).then((query: any) => {
                if (query.nModified === 1) {
                    markNextMsgAsRead().then();
                } else {
                    // send socket message
                    SocketManagement.sendToUser(idOther, '[read]');
                    return new ApiResponse(200, {success: true});
                }
            });
        }

        return markNextMsgAsRead();
    }

    /**
     * count unread messages from user
     * @param user
     */
    public getUnreadCount(user: IUserData): Promise<any> {
        // TODO: proper mongoose query
        return ChatModel.find({
            $or: [{
                'userTwo': user._id
            }, {
                'userOne': user._id
            }]
        }).then((chats: IChatData[]) => {
            let c = 0;
            for (let chat of chats) {
                for (let msg of chat.messages) {
                    if (msg.to == user._id && msg.read === false) {
                        c++;
                    }
                }
            }

            return new ApiResponse(200, {count: c});
        });
    }

    /**
     * add message to chat & save chat
     * @param chat
     * @param message
     * @returns {Promise<IChatData>}
     */
    private static saveChat(chat: IChatData, message: MessageData): Promise<IChatData> {
        // add message
        chat.messages.push(message);

        return ChatModel.findByIdAndUpdate(chat._id, chat, {'new': true}).populate({
            path: 'kutsche',
            select: 'stops date seats guests user'
        }).populate({
            path: 'userOne userTwo',
            select: 'publicInfo.name publicInfo.image'
        });
    }

    /**
     * create most used ApiResponses from database call
     * @param dataManipulation
     * @returns {Promise<ApiResponse>}
     */
    private basicResponse(dataManipulation: Promise<any>): Promise<ApiResponse> {
        return dataManipulation.then(
            (docs: any) => {
                return new ApiResponse(200, docs);
            },
            (err: any) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * add message to chat / create new chat and add message
     * @param message
     * @param kutsche
     * @param user
     * @returns {Promise<ApiResponse>}
     */
    public add(message: MessageData, kutsche: string, user: IUserData): Promise<ApiResponse> {
        let sender = user._id;
        let receiver = message.to;
        message.from = user._id;
        // prepare message
        message = ChatManagement.prepareMessage(message);
        // send socket message
        SocketManagement.sendToUser(message.to, message);

        return ChatManagement.getChat(sender, receiver, user).then(
            (chat: IChatData) => {
                // new chat
                if (!chat || !chat.messages) {
                    return this.basicResponse(ChatManagement.newChat(sender, receiver, kutsche, message, user));
                } else {
                    // assign updated kutsche
                    if (kutsche.length > 4) {
                        chat.kutsche = kutsche;
                    }
                    return this.basicResponse(ChatManagement.saveChat(chat, message));
                }
            },
            (err: Error) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * get chat by id from other user and me
     * @param userId: other
     * @param user: me
     * @returns {Promise<ApiResponse>}
     */
    public getById(userId: string, user: IUserData): Promise<ApiResponse> {
        let dataFetch = ChatManagement.getChat(userId, user._id, user);
        return this.basicResponse(dataFetch);
    }

    /**
     * get all chats from me
     * @param user: me
     * @returns {Promise<ApiResponse>}
     */
    public getAll(user: IUserData): Promise<ApiResponse> {
        let dataFetch = ChatManagement.getChats(user._id);
        return this.basicResponse(dataFetch);
    }
}