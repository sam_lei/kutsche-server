import { ILocationData, LocationData } from "../Modules/Location/LocationData";
/**
 * custom methods for distance calculation
 */
export class DistanceMethods {
    public static C02Factor: number = 0.19;
    public static MoneyFactor: number = 0.2;

    /**
     * calculate trigonometrical distance between two locations in km
     * @param location1
     * @param location2
     * @returns {number}
     */
    public static getDistance(location1: LocationData, location2: LocationData): number {
        let earthRadius = 6378.137; // km
        let dLat = location2.lat * Math.PI / 180 - location1.lat * Math.PI / 180;
        let dLon = location2.long * Math.PI / 180 - location1.long * Math.PI / 180;
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(location1.lat * Math.PI / 180) * Math.cos(location2.lat * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = earthRadius * c;

        return d * 1.33;
    }
}