import {mongoose} from "../../Services/Database";
import {MessageData} from "./Message/MessageData";

export interface ChatData {
    userOne: string;
    userTwo: string;
    kutsche?: string;
    messages: MessageData[];
}

export interface IChatData extends ChatData, mongoose.Document { }