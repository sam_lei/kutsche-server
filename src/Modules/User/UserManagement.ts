import * as UserModel from '../../Modules/User/UserModel';
import { sign, verify, TokenExpiredError } from 'jsonwebtoken';
import * as fs from 'fs';
import * as im from 'imagemagick';
import { DateMethods } from "../../Services/DateMethods";
import { ApiResponse } from "../../Services/ApiResponse";
import { UserTokenData } from "./Token/UserTokenData";
import { IUserData, UserData } from "./UserData";
import { EmailManagement } from "../Email/EmailManagement";
import { environments } from "../../../environments";
import { UserCarData } from "./Car/UserCarData";

export class UserManagement {
    private cert: Buffer;

    constructor() {
        // get private key (secret)
        this.cert = fs.readFileSync(environments.cert);
    }

    private sendWelcomeEmail(email: string, name: string): void {
        UserModel.count().then((count: number) => {
            let subject: string = 'Willkommen bei kutschen.org';
            let message: string = 'Hallo ' + name + ', \n\n';
            message += 'Wir freuen uns, Sie als ' + (count + 1) + '. Nutzer begrüßen zu dürfen. \n\n';
            message += 'Gute Fahrt, \n';
            message += 'Ihr Kutschen Team';
            EmailManagement.sendEmail(email, subject, message);
        });
    }

    private sendRestoreEmail(email: string, name: string, token: string): void {
        let subject: string = 'Passwort Wiederherstellen für kutschen.org';
        let message: string = 'Hallo ' + name + ', \n\n';
        message += 'bitte benutzen Sie folgenden Link um ein neues Passwort zu setzen: https://kutschen.org/neues-passwort/' + token + '\n\n';
        message += 'Dieser Link ist eine Stunde gültig, \n';
        message += 'falls Sie keine Wiederherstellung angefordert haben, ignorieren Sie bitte diese E-Mail. \n\n';
        message += 'Gute Fahrt, \n';
        message += 'Ihr Kutschen Team';
        EmailManagement.sendEmail(email, subject, message);
    }

    /**
     * create most used ApiResponses from database call
     * @param dataManipulation
     * @returns {Promise<ApiResponse>}
     */
    private basicResponse(dataManipulation: Promise<any>): Promise<ApiResponse> {
        return dataManipulation.then(
            (docs: any) => {
                return new ApiResponse(200, docs);
            },
            (err: any) => {
                return new ApiResponse(500, err);
            });
    }

    /**
     * create new token and return user with auth data
     * @param user
     * @param hours
     * @returns {{user: {username: string, publicInfo: UserDataInfo, privateInfo: UserDataInfo}, auth: {token: string, issued: Date, expires: Date}}}
     */
    private newToken(user: IUserData): UserTokenData {
        let now = new Date();
        let toSign = {
            _id: user._id,
            email: user.email,
        };
        let token: string = sign(toSign, this.cert, {
            algorithm: 'HS256',
            expiresIn: environments.tokenValidTime + "s"
        });
        let auth = {
            token: token,
            issued: now,
            expires: DateMethods.addSeconds(now, environments.tokenValidTime)
        };
        return {
            user: UserManagement.toPrivate(user),
            auth: auth
        }
    }

    private static toPublic(user: IUserData): IUserData {
        user = UserManagement.toPrivate(user);
        user.privateInfo = {
            name: '',
        };
        user.email = "";
        return user;
    }

    private static toPrivate(user: IUserData): IUserData {
        user.salt = "";
        user.hash = "";
        return user;
    }

    private updateLastLogin(user: IUserData): Promise<ApiResponse> {
        user.lastLoggedIn = new Date();
        let dataUpdate = UserModel.findByIdAndUpdate(user._id, user, {'new': true}); // new flag returns modified user
        return this.basicResponse(dataUpdate);
    }

    /**
     * create new user with initial data
     * @param email
     * @param name
     * @returns {{username: string, verified: boolean, privateInfo: {image: string}, publicInfo: {image: string}}}
     */
    public static newUser(email: string, name: string): UserData {
        let placeholderImage = '';
        return {
            email: email,
            verified: false,
            kutscheServedCount: 0,
            kutscheTakenCount: 0,
            ratings: [],
            privateInfo: {
                image: placeholderImage,
                name: name
            },
            publicInfo: {
                image: placeholderImage,
                name: name
            }
        }
    }

    public updateUser(user: IUserData): Promise<ApiResponse> {
        let dataUpdate = UserModel.findByIdAndUpdate(user._id, user, {'new': true});
        return this.basicResponse(dataUpdate);
    }

    /**
     * register new user
     * @param email
     * @param name
     * @param password
     * @returns {Promise<ApiResponse>}
     */
    public register(email: string, name: string, password: string): Promise<ApiResponse> {
        let newUser: UserData = UserManagement.newUser(email, name);
        let newUserModel = new UserModel(newUser);
        return new Promise<ApiResponse>((resolve) => {
            UserModel.register(newUserModel, password, (err: Error) => {
                if (err) {
                    resolve(new ApiResponse(409, err));
                } else {
                    this.login(email, password).then(apiResponse => {
                        resolve(apiResponse);
                        this.sendWelcomeEmail(email, name);
                    });
                }
            });
        });
    }

    /**
     * login new user
     * @param email
     * @param password
     * @returns {Promise<ApiResponse>}
     */
    public login(email: string, password: string): Promise<ApiResponse> {
        let response: ApiResponse;
        let pw: Buffer = password ? new Buffer(password, 'binary') : new Buffer('', 'binary');

        return new Promise<ApiResponse>((resolve) => {
            UserModel.authenticate()(email, pw, (err: any, user: IUserData) => {
                if (err)
                    response = new ApiResponse(409, err);
                if (!user)
                    response = new ApiResponse(403, 'login failed');
                else {
                    this.updateLastLogin(user);
                    response = new ApiResponse(200, this.newToken(user));
                }
                resolve(response);
            });
        });
    }

    /**
     * Acces token check for middleware
     * @param token
     * @returns {Promise<ApiResponse>}
     */
    public verifyToken(token: string): Promise<ApiResponse> {
        let response: ApiResponse;
        return new Promise<ApiResponse>((resolve) => {
            if (!token) {
                response = new ApiResponse(401, 'token required');
                resolve(response);
            }
            else {
                verify(token, this.cert, {algorithms: ['HS256']}, (err, payload) => {
                    if (err instanceof TokenExpiredError)
                        response = new ApiResponse(403, 'token expired');
                    else if (err)
                        response = new ApiResponse(403, 'invalid token');
                    else
                        response = new ApiResponse(200, payload);
                    resolve(response);
                });
            }
        });
    }

    public addRestoreToken(email: string): Promise<ApiResponse> {
        console.log(email);
        return UserModel.findByUsername(email).then((user: IUserData) => {
            if (!user) return new ApiResponse(404, 'email existiert nicht');

            let token = require('crypto').randomBytes(16).toString('hex');
            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

            this.sendRestoreEmail(email, user.privateInfo.name, token);
            // UserModel.findByIdAndUpdate(user._id, user, {'new': true});
            user.save();

            return new ApiResponse(200, {});
        }, (err: Error) => {
            return new ApiResponse(404, err);
        });
    }

    public verifyRestoreToken(token: string, newPassword: string): Promise<ApiResponse> {
        return UserModel.findOne({
            resetPasswordToken: token,
            resetPasswordExpires: {
                $gt: Date.now()
            }
        }).then((user: IUserData) => {
            if (!user) return new ApiResponse(500, 'Code falsch oder abgelaufen');

            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;
            user.setPassword(newPassword, () => user.save());

            return new ApiResponse(200, 'success');
        }, (err: Error) => {
            return new ApiResponse(500, err);
        });
    }

    public getUser(id: string, currentUser: IUserData): Promise<ApiResponse> {
        return UserModel.findById(id).populate({
            path: 'ratings.from',
            select: 'publicInfo.image publicInfo.name'
        }).then((user: IUserData) => {
            let preparedUser;
            if (currentUser && user._id == currentUser._id)
                preparedUser = UserManagement.toPrivate(user);
            else
                preparedUser = UserManagement.toPublic(user);
            return new ApiResponse(200, preparedUser)
        }, (err: Error) => {
            return new ApiResponse(500, err)
        });
    }

    public getPrivateUser(id: string): Promise<IUserData> {
        return UserModel.findById(id);
    }

    public putUser(user: IUserData, id: string): Promise<ApiResponse> {
        return UserModel.findByIdAndUpdate(id, user).then(
            (user: IUserData) => {
                return new ApiResponse(204)
            },
            (err: Error) => {
                return new ApiResponse(500, err)
            });
    }

    public static increaseKutscheTakenCount(user: IUserData): void {
        UserModel.findByUsername(user.email).then((user: IUserData) => {
            user.kutscheTakenCount++;
            user.save();
        });
    }

    public static decreaseKutscheTakenCount(user: IUserData): void {
        UserModel.findByUsername(user.email).then((user: IUserData) => {
            user.kutscheTakenCount--;
            user.save();
        });
    }

    public static increaseKutscheServedCount(user: IUserData): void {
        UserModel.findByUsername(user.email).then((user: IUserData) => {
            user.kutscheServedCount++;
            user.save();
        });
    }

    public static decreaseKutscheServedCount(user: IUserData): void {
        UserModel.findByUsername(user.email).then((user: IUserData) => {
            user.kutscheServedCount--;
            user.save();
        });
    }

    public updateCar(user: IUserData, car: UserCarData): Promise<ApiResponse> {
        return new Promise<ApiResponse>((resolve) => {
            user.car = car;
            resolve(this.updateUser(user));
        });
    }

    public uploadPicture(file: any, user: IUserData): Promise<ApiResponse> {
        return new Promise<ApiResponse>((resolve) => {
            // uid for prevent caching
            let uid: string = Math.random().toString(36).slice(2);
            let path: string = user._id + '__' + uid + '.jpg';
            if (!file) resolve(new ApiResponse(500, 'undefined file'));
            // process image
            // console.log('file.path: ' + file.path);
            // console.log('destination: ' + path);
            im.crop({
                srcPath: file.path,
                dstPath: environments.photos + path,
                quality: 0.8,
                sharpening: 0.2,
                format: 'jpg',
                // gravity: 'Center',
                width: 200,
                height: 200
            }, (err: Error) => {
                // upload error
                try {
                    // remove file if created
                    fs.unlinkSync(file.path);
                } catch (e) {
                    console.log(e);
                }
                if (err) {
                    console.log("image upload ERROR");
                    console.log(err);
                    resolve(new ApiResponse(500, err));
                } else {
                    this.getPrivateUser(user._id).then(user => {
                        // add new path to user
                        if (user.privateInfo) {
                            // delete old photo
                            try {
                                fs.unlinkSync(environments.photos + user.privateInfo.image);
                            } catch (e) {
                                console.log(e);
                            }
                        }
                        user.publicInfo.image = path;
                        user.privateInfo.image = path;
                        console.log("new image path = " + user.publicInfo.image);
                        resolve(this.updateUser(user));
                    });
                }
            });
        });
    }
}