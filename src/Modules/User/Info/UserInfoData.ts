import {LocationData} from "../../Location/LocationData";

export interface UserDataInfo {
    image?: string;
    email?: string;
    name: string;
    telegram?: string;
    adress?: LocationData;
    gender?: string;
    birthDate?: Date;
}