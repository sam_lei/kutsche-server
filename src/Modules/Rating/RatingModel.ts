import {mongoose} from "../../Services/Database";
import {IRatingData} from "./RatingData";
import {RatingSchema} from "./RatingSchema";

let RatingModel;

try {
    // Throws an error if 'Name' hasn't been registered
    RatingModel = mongoose.model('Rating')
} catch (e) {
    RatingModel = mongoose.model<IRatingData>('Rating', RatingSchema);
}

export = RatingModel;