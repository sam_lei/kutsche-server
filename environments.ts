const dir = __dirname;

export const environments = {
  cert: dir + '/Config/Dev/Private.key',
  photos: dir + '/photos/',
  tokenValidTime: 60 * 60 * 24 * 7 // seconds
};