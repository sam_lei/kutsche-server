import {Router, Request, Response} from 'express';
import {UserRouter} from './UserRouter';
import {KutscheManagement} from "../Modules/Kutsche/KutscheManagement";
import {LocationManagement} from "../Modules/Location/LocationManagement";
/**
 * kutsche
 */
export class KutscheRouter {
    private router: Router = Router();

    getRouter(): Router {
        let kutscheManagement = new KutscheManagement();
        let userRouter = new UserRouter();
        let locationManagement = new LocationManagement();

        this.router.post('/', userRouter.verifyToken, (request: Request, response: Response) => {
            kutscheManagement.add(request.body, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/', (request: Request, response: Response) => {
            kutscheManagement.getAll().then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.delete('/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            kutscheManagement.remove(request.params.id, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.put('/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            kutscheManagement.update(request.params.id, request.body, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/:id', (request: Request, response: Response) => {
            kutscheManagement.getById(request.params.id).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/user/:id', (request: Request, response: Response) => {
            kutscheManagement.getByUserId(request.params.id).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/guest/:id', (request: Request, response: Response) => {
            kutscheManagement.getByGuestId(request.params.id).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/date/:date', (request: Request, response: Response) => {
            kutscheManagement.getByDate(request.params.date).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/future/:days', (request: Request, response: Response) => {
            kutscheManagement.getByFuture(parseInt(request.params.days)).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/statistics/all', (request: Request, response: Response) => {
            kutscheManagement.getStatistics().then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.put('/guest/:kutscheId/:guestId', userRouter.verifyToken, (request: Request, response: Response) => {
            kutscheManagement.addGuest(request.params.kutscheId, request.params.guestId, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.delete('/guest/:kutscheId/:guestId', userRouter.verifyToken, (request: Request, response: Response) => {
            kutscheManagement.removeGuest(request.params.kutscheId, request.params.guestId, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.post('/filter', (request: Request, response: Response) => {
            // console.log(request.body);
            kutscheManagement.getByFilter(request.body).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });


        return this.router;
    }
}

