﻿import {Router, Request, Response, NextFunction} from 'express';
import {ApiResponse} from "../Services/ApiResponse";
import {UserManagement} from "../Modules/User/UserManagement";
import * as multer from "multer";

/**
 * user
 */
export class UserRouter {
    private router: Router = Router();
    private upload = multer({
        // storage: multer.memoryStorage()
        dest: 'uploads/'
    });


    /**
     * Authentication middleware
     * @param request
     * @param response
     * @param next
     */
    public verifyToken(request: Request, response: Response, next: NextFunction) {
        let userManagement = new UserManagement();
        let token = request.headers['authorization'] || "";
        token = token.toString();

        userManagement.verifyToken(token).then((apiResponse: ApiResponse) => {
            if (apiResponse.isSuccess()) {
                request.user = apiResponse.getData();
                next();
            } else {
                apiResponse.addToResponse(response);
            }
        });
    }

    getRouter(): Router {
        let userManagement = new UserManagement();

        this.router.post('/register', (request: Request, response: Response) => {
            let password = request.body.password;
            let email = request.body.email;
            let name = request.body.name;

            userManagement.register(email, name, password).then(apiResponse => {
                apiResponse.addToResponse(response, "Register: " + email + ", " + password);
            });
        });

        this.router.post('/login', (request: Request, response: Response) => {
            let password = request.body.password;
            let email = request.body.email;

            userManagement.login(email, password).then(apiResponse => {
                apiResponse.addToResponse(response, "Login: " + email + ", " + password);
            });
        });

        this.router.post('/restore', (request: Request, response: Response) => {
            let email = request.body.email;

            userManagement.addRestoreToken(email).then(apiResponse => {
                apiResponse.addToResponse(response, "Restore: " + email);
            });
        });

        this.router.post('/restore/:token', (request: Request, response: Response) => {
            let newPassword = request.body.password;
            let token = request.params.token;

            userManagement.verifyRestoreToken(token, newPassword).then(apiResponse => {
                apiResponse.addToResponse(response, "Verify: " + request.params.token + ', ' + newPassword);
            });
        });

        this.router.put('/', this.verifyToken, (request: Request, response: Response) => {
            userManagement.putUser(request.body, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.put('/image', this.verifyToken, this.upload.single('uploadFile'), (request: Request, response: Response) => {
            userManagement.uploadPicture(request.file, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.put('/car', this.verifyToken, (request: Request, response: Response) => {
            console.log(request.body);
            userManagement.updateCar(request.user, request.body).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/:id', this.verifyToken, (request: Request, response: Response) => {
            userManagement.getUser(request.params.id, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        return this.router;
    }
}

