import {mongoose} from "../../Services/Database";
import {IKutscheData} from "./KutscheData";
import {KutscheSchema} from "./KutscheSchema";

let KutscheModel;

try {
    // Throws an error if 'Name' hasn't been registered
    KutscheModel = mongoose.model('Kutsche')
} catch (e) {
    KutscheModel = mongoose.model<IKutscheData>('Kutsche', KutscheSchema);
}

export = KutscheModel;