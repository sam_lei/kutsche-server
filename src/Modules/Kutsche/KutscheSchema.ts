import {mongoose} from "../../Services/Database";
import {LocationSchema} from "../Location/LocationSchema";

export const KutscheSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    date: {
        type: Date,
        required: true
    },
    stops: {
        type: [LocationSchema],
        required: true
    },
    guests: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: false
        }
    ],
    seats: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    price: {
        type: Number,
        required: true
    }
});