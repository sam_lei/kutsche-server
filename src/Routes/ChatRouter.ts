import {Router, Request, Response} from 'express';
import {UserRouter} from './UserRouter';
import {ChatManagement} from "../Modules/Chat/ChatManagement";

/**
 * chat
 */
export class ChatRouter {
    private router: Router = Router();

    getRouter(): Router {
        let chatManagement = new ChatManagement();
        let userRouter = new UserRouter();

        this.router.post('/', userRouter.verifyToken, (request: Request, response: Response) => {
            chatManagement.add(request.body.message, request.body.kutsche, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/', userRouter.verifyToken, (request: Request, response: Response) => {
            chatManagement.getAll(request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/read/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            chatManagement.markChatAsRead(request.params.id, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/unread', userRouter.verifyToken, (request: Request, response: Response) => {
            chatManagement.getUnreadCount(request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        this.router.get('/:id', userRouter.verifyToken, (request: Request, response: Response) => {
            chatManagement.getById(request.params.id, request.user).then(apiResponse => {
                apiResponse.addToResponse(response);
            });
        });

        return this.router;
    }
}

