import {mongoose} from "../../Services/Database";
import {MessageSchema} from "./Message/MessageSchema";

export const ChatSchema = new mongoose.Schema({
    userOne: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    userTwo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    kutsche: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Kutsche',
        required: false
    },
    messages: [MessageSchema]
});